(ns exam-clj.core-test
  (:require [clojure.test :refer :all]
            [clojure.core.async :as async :refer [<! >! <!! >!! chan go go-loop]]
            [exam-clj.core :refer :all])
  (:import (java.util ArrayList)))

(deftest number-store-test
  (testing "Store to Number in Java ArrayList"
    (let [lst (ArrayList.)
          mutex (Object.)]
      (dotimes [_ 10] (store-exec lst mutex))
      (is (= (.size lst) 10))
      (doseq [l lst]
        (is (and (>= l 0) (< l 10)))))))

(deftest generate-json-hashmap
  (testing "Generate Hashmap to init"
    (let [json (gen-counts-map)]
      (are [n] (= n 0)
               (.get json "0")
               (.get json "1")
               (.get json "2")
               (.get json "3")
               (.get json "4")
               (.get json "5")
               (.get json "6")
               (.get json "7")
               (.get json "8")
               (.get json "9")))))

(deftest aggregate-process-test
  (<!!
    (let [all-done-chan (async/chan)
          n 1 m 1]
      (reset! child-chs (assoc @child-chs n (chan)))
      (start-aggregate-process @child-chs)
      (testing "Ping From aggregate-ch"
        (go []
            (dotimes [_ 2]
              (Thread/sleep 1000)
              (let [p (<! (@child-chs n))]
                (is (= p ping)))
              (>! aggregate-ch {:n 1 :store [1]}))
            (async/close! all-done-chan))))))

;; chに何も書き込まれないパターンは今のテストだとずっと待ってしまうので難しい
#_(deftest child-process-test-kill
  (<!!
    (let [all-done-chan (async/chan)
          n 2 m 1]
      (testing "Send to kill message"
        (reset! child-chs (assoc @child-chs n (chan)))
        (start-child-process n m)
        (go []
            ;; killを送るとスレッドを止めるのでclear後何秒待ってもstoreは空のハズ
            (>! (@child-chs n) kill)
            (Thread/sleep 1000)
            ;; killされると何も戻ってこなくなる
            (let [{:keys [store]} (<! aggregate-ch)]
              (is (= 0 (.size store))))
            (async/close! all-done-chan))))))

(deftest child-process-test-clear
  (<!!
    (let [all-done-chan (async/chan)
          n 3 m 1]
      (testing "Send to clear message"
        (reset! child-chs (assoc @child-chs n (chan)))
        (start-child-process n m)
        (go []
            (>! (@child-chs n) clear)
            (>! (@child-chs n) ping)
            (let [{:keys [store]} (<! aggregate-ch)]
              (is (= (.size store) 0)))
            (async/close! all-done-chan))))))

(deftest child-process-test-ping
  (<!!
    (let [all-done-chan (async/chan)
          n 4 m 1]
      (testing "Send to ping message"
        (reset! child-chs (assoc @child-chs n (chan)))
        (start-child-process n m)
        (go []
            (Thread/sleep 1000)
            (>! (@child-chs n) ping)
            (let [{:keys [store]} (<! aggregate-ch)]
              ;; 1秒スリープしたのでいくつか要素が入っていること
              (is (> (.size store) 0))
              (doseq [s store]
                ;; 0以上10以下の数だけ生成されていること
                (is (and (>= s 0) (< s 10)))))
            (async/close! all-done-chan))))))
