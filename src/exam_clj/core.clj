(ns exam-clj.core
  (:require [clojure.core.async :as async :refer [<! >! <!! chan go go-loop]]
            [cheshire.core :as cheshire])
  (:gen-class)
  (:import (java.util HashMap ArrayList)))

;; TODO: 全体的にgo-loopの処理を関数に外出しないとテストがC版より難しい

;; 集計用チャネル
;; チャネルの役割はforkのファイルディスクリプタみたいなもの
(def aggregate-ch (chan))

;; 各スレッドを立ち上げるプロセスとの通信用チャネル
(def child-chs (atom {}))

(def kill :kill)
(def clear :clear)
(def ping :ping)
;; pidsの数によるスレッド分割数
(def thread-sep 4)

;; 初期化用マップ生成関数
(defn gen-counts-map []
  (HashMap. {(str 0) 0
             (str 1) 0
             (str 2) 0
             (str 3) 0
             (str 4) 0
             (str 5) 0
             (str 6) 0
             (str 7) 0
             (str 8) 0
             (str 9) 0}))

;; テストのために切り出し
(defn store-exec [store mutex]
  (locking mutex
    (.add store (rand-int 10))))

(defn gen-rand [n m store mutex]
  "0.1秒間隔で0から9までのいずれかの数値を発行し、storeに蓄える関数"
  (fn []
    (store-exec store mutex)
    ;; TODO: スレッドから直接集計プロセスに対してメッセージングしたほうが安全な気がするが趣旨と異なる気もする
    #_(go [] (>! aggregate-ch (rand-int 10)))
    (Thread/sleep 10)
    (recur)))

(defn start-child-process [n m ch]
  "m個スレッド立ち上げるgo-loopを起動する"
  (let [store (ArrayList.)
        mutex (Object.)
        threads (for [x (range m)] (Thread. (gen-rand n x store mutex)))]
    (doseq [thread threads]
      (.start thread))
    (go-loop []
      (try
        (if-let [data (<! ch)]
          ;; 読み取ったメッセージのフラグに応じて処理を行う
          (cond
            (= kill data) (doseq [thread threads] (.stop thread))
            (= clear data) (locking mutex (.clear store))
            (= ping data) (do
                            ;; 参照を返すとこの後のclear処理で空にされるのでコピーを作って対応
                            (>! aggregate-ch {:n n :store (into [] (ArrayList. store))})
                            (locking mutex
                              (.clear store)))
            :else (println "invalid  messages")))
        (catch IllegalMonitorStateException e
          (println (str "in process " n ", caught exception: " (.getMessage e)))))
      (recur))))

(defn ch-get-in-thread [pids effective-fn]
  (fn []
    (let [all-done-chan (async/chan)]
      ;; goブロックを同期する
      ;; これをしないとthread.joinにすぐ行ってしまう
      (<!!
        (go []
            (when (not (empty? pids))
              (doseq [process pids]
                (>! (second process) ping)
                ;; pingの結果を同期的に待ち受ける
                (let [{:keys [store]} (<! aggregate-ch)]
                  (doseq [x store]
                    ;; 副作用ありの関数を実行
                    (effective-fn x)))))
            (async/close! all-done-chan))))))

(defn start-aggregate-process [pids]
  (let [partition-pids (partition-all thread-sep pids)]
    (go-loop []
      ;; 開始時刻から1秒間に生成されたものを取るので開始日時の取得を先に行いあとで待つ
      (let [ret (HashMap. {"time" (quot (System/currentTimeMillis) 1000)})]
        ;; 1秒待つ
        (Thread/sleep 1000)
        ;; ここは同期的に動くはず
        ;; pidのデカさによってthread処理にすればまたなくて済むかも？
        (let [zero (atom 0) one (atom 0) two (atom 0) three (atom 0) four (atom 0)
              five (atom 0) six (atom 0) seven (atom 0) eight (atom 0) nine (atom 0)
              effective-fn #(cond
                              (= % 0) (locking zero (swap! zero inc))
                              (= % 1) (locking one (swap! one inc))
                              (= % 2) (locking two (swap! two inc))
                              (= % 3) (locking three (swap! three inc))
                              (= % 4) (locking four (swap! four inc))
                              (= % 5) (locking five (swap! five inc))
                              (= % 6) (locking six (swap! six inc))
                              (= % 7) (locking seven (swap! seven inc))
                              (= % 8) (locking eight (swap! eight inc))
                              (= % 9) (locking nine (swap! nine inc)))
              ;; スレッド立ち上げのコストを考えると違うやり方のほうが良いかも
              threads (for [x partition-pids] (Thread. (ch-get-in-thread x effective-fn)))]
          ;; スレッド起動後待機する
          (doseq [t threads]
            (.start t))
          (doseq [t threads]
            (.join t))
          (.put ret "counts" (HashMap. {(str 0) @zero
                                        (str 1) @one
                                        (str 2) @two
                                        (str 3) @three
                                        (str 4) @four
                                        (str 5) @five
                                        (str 6) @six
                                        (str 7) @seven
                                        (str 8) @eight
                                        (str 9) @nine}))
          (doseq [t threads]
            (.stop t)))
        (println (cheshire/generate-string ret)))
      (recur))))

(defn start-process [n m]
  ;; スレッド管理用プロセス起動 nプロセスでmスレッド立ち上げる
  (dotimes [pid n]
    ;; STMとか使っているがchの生成はメインプロセスで行なわれるので何も気にしなくて良い
    (reset! child-chs (assoc @child-chs pid (chan)))
    (start-child-process pid m (@child-chs pid)))
  ;; 集計用のプロセス起動
  (start-aggregate-process @child-chs)

  ;; 停止用関数を返す
  (fn
    ([]
     (go []
         (doseq [process @child-chs]
           (>! (second process) kill))))
    ([n]
     (go []
         (>! (@child-chs n) kill)))))

(defn -main
  "第一引数がn,第二引数がm"
  [& args]
  (if (< 0 (count args))
    (start-process (Integer/parseInt (first args)) (Integer/parseInt (second args)))
    (start-process 4 4)))

