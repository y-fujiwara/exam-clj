# exam-clj

## Requirement

* java8
* leiningen

## Usage

### RUN

* `$ lein deps`
* `$ lein run $PROCESS_NUM $THREAD_NUM`
  * ex. `$ lein run 4 4`
    * 4 threads in 4 process

### TEST

* `$ lein test`
  * NOTICE: test is incomplete
    
    
    

